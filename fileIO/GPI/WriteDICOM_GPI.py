# Author: Ryan Robison
# Date: 2017 Jul 12

import os
import time
import gpi
import numpy as np

class ExternalNode(gpi.NodeAPI):
    """Write Dicom data out to file
    
    INPUT: Dicom file 

    WIDGETS:
    I/O Info - Gives info on data file and data type
    File Browser - button to launch file browser, and typein widget if the pathway is known.
    """

    def execType(self):
        # default executable type
        # return gpi.GPI_THREAD
        return gpi.GPI_PROCESS # this is the safest
        # return gpi.GPI_APPLOOP

    def initUI(self):

        # Widgets
        # self.addWidget('PushButton', 'Write All Images', toggle = True, val=0)
        self.addWidget('PushButton', 'Write All Images', toggle = True, val=0)
        self.addWidget('ComboBox', 'Image', items=[])
        self.addWidget('PushButton', 'User Defined File Name', toggle = True, val=0)
        self.addWidget('SaveFileBrowser', 'File Browser',
                button_title='Browse', caption='Write Dicom',
                filter='(IM* *.dcm)')
        self.addWidget('PushButton', 'Write', toggle = False, val=0)

        # IO Ports
        self.addInPort(title='Images', type='NPYarray', ndim=3)
        self.addInPort(title='DicomDict', type='DICT')

        self.URI = gpi.TranslateFileURI

    def validate(self):
        import gpidicom.fileIO.dicomlib as dcm
        import imp
        imp.reload(dcm)
        udef = self.getVal('User Defined File Name')
        writeall = self.getVal('Write All Images')
        if udef:
            fname = self.URI(self.getVal('File Browser'))
            self.setDetailLabel(fname)
        else:
            self.setAttr('File Browser', filter=None)

        if writeall:
            self.setAttr('Image', visible = False)
        else:
            keys = self.getData('DicomDict').keys()
            self.setAttr('Image', visible = True)
            images = self.getAttr('Image', 'items')
            Nimages = len(keys)
            if ('DicomDict' in self.portEvents() or len(images) != Nimages): 
                images = list(keys)
                self.setAttr('Image', items = images)

    def compute(self):

        import os
        import time
        import numpy as np
        import re
        import gpidicom.fileIO.dicomlib as dcm
        import pydicom

        data = self.getData('Images')
        hdr = self.getData('DicomDict')
        udef = self.getVal('User Defined File Name')
        writeall = self.getVal('Write All Images')
        write = self.getVal('Write')

        if (data.shape[0] != len(hdr)):
            self.log.warn("number of images must match number of Dicom header entries")
            return 1

        if write:
            fname = self.URI(self.getVal('File Browser'))
            if udef:
                if writeall:
                    index = 0
                    for img in hdr.keys():
                        filename = fname+str(index+1).zfill(4)
                        dat = data[index]
                        ds = dcm.dict_to_data_set(dat, hdr[img])
                        ds.save_as(filename, write_like_original=False)
                        index = index+1
                else:
                    filename = fname
                    img = self.getVal('Image')
                    index = self.getAttr('Image', 'index')
                    dat = data[index]
                    ds = dcm.dict_to_data_set(dat, hdr[img])
                    ds.save_as(filename, write_like_original=False)
            else:
                basedir = os.path.dirname(fname)
                if writeall:
                    index = 0
                    for img in hdr.keys():
                        filename = basedir+'/'+img
                        dat = data[index]
                        ds = dcm.dict_to_data_set(dat, hdr[img])
                        ds.BitsAllocated = int(ds.BitsAllocated)
                        ds.save_as(filename, write_like_original=False)
                        index = index+1
                else:
                    img = self.getVal('Image')
                    filename = basedir+'/'+img
                    index = self.getAttr('Image', 'index')
                    dat = data[index]
                    ds = dcm.dict_to_data_set(dat, hdr[img])
                    ds.BitsAllocated = int(ds.BitsAllocated)
                    ds.save_as(filename, write_like_original=False)

        return(0)
